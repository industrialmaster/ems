/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.im.ems.control;

import com.im.ems.model.Category;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JOptionPane;

/**
 *
 * @author Nadun Shyn
 */
public class CategoryController {
    static public void save(Category category){
        try {
            
            //1. Create SQL Statement
            String sql = "INSERT INTO category (name) VALUES (?)";
            
            //2. Database Connection 
            Class.forName("com.mysql.jdbc.Driver");
            String url = "jdbc:mysql://localhost:3306/dbexpense";
            String un = "root";
            String pw = "0147";
            Connection con = DriverManager.getConnection(url, un, pw);
            
            //3. Prepare Statement , Fill, Execute
            PreparedStatement ps = con.prepareStatement(sql);
            ps.setString(1, category.getName());
            ps.executeUpdate();
            
            
            
            
        } catch (Exception e) {
            e.printStackTrace();
            JOptionPane.showMessageDialog(null, "Error\n"+e.getMessage());
        }
    }
    
    static public void update(Category category){
        try {
            
            //1. Create SQL Statement
            String sql = "UPDATE category SET name=?  WHERE id=?";
            
            //2. Database Connection 
            Class.forName("com.mysql.jdbc.Driver");
            String url = "jdbc:mysql://localhost:3306/dbexpense";
            String un = "root";
            String pw = "0147";
            Connection con = DriverManager.getConnection(url, un, pw);
            
            //3. Prepare Statement , Fill, Execute
            PreparedStatement ps = con.prepareStatement(sql);
            ps.setString(1, category.getName());
            ps.setInt(2, category.getId());
            ps.executeUpdate();
            
            
            
            
        } catch (Exception e) {
            e.printStackTrace();
            JOptionPane.showMessageDialog(null, "Error\n"+e.getMessage());
        }
    }
    
    static public void delete(Category category){
        
    }
    static public List<Category> list(){
        
        List<Category> list = new ArrayList<Category>();
        try {
            
            //1. Create SQL Statement
            String sql = "SELECT * FROM category";
            
            //2. Database Connection 
            Class.forName("com.mysql.jdbc.Driver");
            String url = "jdbc:mysql://localhost:3306/dbexpense";
            String un = "root";
            String pw = "0147";
            Connection con = DriverManager.getConnection(url, un, pw);
            
            //3. Get Result Create Objects and Add them to List
            
            ResultSet rs = con.createStatement().executeQuery(sql);
            while(rs.next()){
                int id = rs.getInt("id");
                String name = rs.getString("name");
                Category category = new Category();
                category.setId(id);
                category.setName(name);
                list.add(category);
            }
            
            
        } catch (Exception e) {
            e.printStackTrace();
            JOptionPane.showMessageDialog(null, "Error\n"+e.getMessage());
        }
        return list;
    }
    
    static public Category get(int id){
        return null;
    }
}
